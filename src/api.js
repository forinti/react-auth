const root = "//react.test/"

const apiRoutes = ["register", "auth", "getuserbytoken"]
let obj = {}
apiRoutes.forEach(item => {
  obj[item] = `${root}${item}`
})

export const api = obj
