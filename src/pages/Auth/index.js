import React, { Component } from "react"
import { Redirect } from "react-router-dom"
import { connect } from "react-redux"
import { auth } from "../../actions/AuthActions"
import Loading from "../../components/Loading"

class Auth extends Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = { login: "", password: "", errors: {} }
  }

  handleChange = e => {
    this.setState({
      [e.currentTarget.name]: e.currentTarget.value
    })
  }

  handleSubmit = e => {
    e.preventDefault()

    this.props.authRequest({
      login: this.state.login,
      password: this.state.password
    })

    this.setState({ hasErrors: true })
  }

  render() {
    const { loggedIn, errors, loading } = this.props.auth
    const { hasErrors } = this.state
    return (
      <div>
        {loggedIn ? (
          <Redirect to="/" />
        ) : loading ? (
          <Loading />
        ) : (
          <form action="/auth">
            <div className="form-group">
              <label htmlFor=""> Login </label>
              <input
                type="text"
                name="login"
                onChange={this.handleChange}
                value={this.state.login}
                className={
                  "form-control " +
                  (typeof errors.login !== "undefined" && hasErrors
                    ? "is-invalid"
                    : "")
                }
              />
              <div className="invalid-feedback">
                {typeof errors.login !== "undefined"
                  ? errors.login.map((item, i) => <div key={i}>{item}</div>)
                  : null}
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="">Password</label>
              <input
                type="password"
                name="password"
                onChange={this.handleChange}
                className={
                  "form-control " +
                  (typeof errors.password !== "undefined" && hasErrors
                    ? "is-invalid"
                    : "")
                }
                value={this.state.password}
              />

              <div className="invalid-feedback">
                {typeof errors.password !== "undefined"
                  ? errors.password.map((item, i) => <div key={i}>{item}</div>)
                  : null}
              </div>
            </div>
            <button className={"btn btn-success"} onClick={this.handleSubmit}>
              Login
            </button>
          </form>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})
const mapDispatchToProps = dispatch => ({
  authRequest: credentials => {
    dispatch(auth(credentials))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth)
