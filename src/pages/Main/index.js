import React, { Component } from "react"
import { connect } from "react-redux"

class Main extends Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      counter: 0,
      touchStart: false
    }
  }
  handleDrag = e => {
    this.setState({
      touchStart: true,
      startX: e.touches[0].screenX,
      startY: e.touches[0].screenY
    })
  }
  componentWillUpdate() {
    return this.state.counter % 3 === 0
  }
  handleTouchend = e => {
    this.setState({ touchStart: false, diffX: 0, diffY: 0 })
  }
  handleTouchMove = e => {
    this.setState({
      diffX: this.state.startX - e.changedTouches[0].screenX,
      diffY: this.state.startY - e.changedTouches[0].screenY,
      counter: this.state.counter + 1
    })
  }
  handleMouseDown
  render() {
    const { user } = this.props.auth
    console.log(user)
    return (
      <div>
        <h1>Hello, {user.login} </h1>
        <div
          onTouchStart={e => {
            if (e.touches.lenght > 1) e.preventDefault()
          }}
        >
          <button
            style={{
              marginTop: `calc(0px - ${this.state.diffY}px)`,
              marginLeft: `calc(0px - ${this.state.diffX}px)`,

              transition: this.state.touchStart ? "" : "all 0.5s ease-in"
            }}
            className="btn btn-success"
            onTouchStart={this.handleDrag}
            onTouchEnd={this.handleTouchend}
            onTouchMove={this.handleTouchMove}
          >
            Drag me
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})
export default connect(mapStateToProps)(Main)
