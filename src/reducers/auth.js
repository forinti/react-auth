import {
  AUTH,
  AUTH_SUCCESS,
  AUTH_FAILURE,
  SET_TOKEN,
  LOG_OUT,
  SET_USER,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAILURE
} from "../actions/AuthActions"

const initialState = {
  loggedIn: false,
  user: null,
  errors: {},
  loading: false,
  token: null,
  hasErrors: false
}

export function authReducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER: {
      return {
        ...state,
        loading: true,
        errors: {}
      }
    }
    case REGISTER_SUCCESS: {
      return {
        ...state,
        loggedIn: true,
        loading: false,
        hasErrors: false,
        errors: {},
        user: action.payload.user
      }
    }
    case REGISTER_FAILURE: {
      return {
        ...state,
        loggedIn: false,
        loading: false,
        hasErrors: true,
        errors: action.payload
      }
    }
    case AUTH: {
      return {
        ...state,
        loading: true,
        errors: {}
      }
    }
    case AUTH_SUCCESS: {
      return {
        ...state,
        loggedIn: true,
        user: action.payload.user,
        errors: {},
        loading: false
      }
    }

    case AUTH_FAILURE: {
      return {
        ...state,
        loggedIn: false,
        hasErrors: true,
        errors: action.payload,
        loading: false
      }
    }

    case SET_TOKEN: {
      return {
        ...state,
        token: action.payload
      }
    }
    case LOG_OUT: {
      return {
        ...state,
        loggedIn: false,
        errors: {}
      }
    }
    case SET_USER: {
      return {
        ...state,
        user: action.payload
      }
    }

    default: {
      return state
    }
  }
}
