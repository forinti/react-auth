import React from "react"
import { Link } from "react-router-dom"

const Nav = props => {
  const { loggedIn } = props
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link to="/" className="navbar-brand animated fadeIn">
        Logo
      </Link>
      <div className="collapse navbar-collapse">
        <div className="navbar-nav">
          {!loggedIn ? (
            <Link to="/login" className="nav-item nav-link">
              Login
            </Link>
          ) : null}
          {!loggedIn ? (
            <Link to="/register" className="nav-item nav-link">
              Register
            </Link>
          ) : null}
        </div>
      </div>
      {loggedIn ? (
        <div className=" navbar-nav ml-auto">
          <form
            action=""
            onSubmit={e => {
              e.preventDefault()
              props.logOutAction()
            }}
          >
            <button className="btn btn-danger">Log out</button>
          </form>
        </div>
      ) : null}
    </nav>
  )
}

export default Nav
