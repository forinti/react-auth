import React from "react"

const Loading = () => (
  <div className="overlay-loader">
    <div className="loader">
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
)
export default Loading
