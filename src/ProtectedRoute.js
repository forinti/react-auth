import React, { Component } from "react"
import { Route, Redirect, withRouter } from "react-router-dom"
import { connect } from "react-redux"

class ProtectedRoute extends Component {
  constructor(props) {
    super(props)
    this.props = props
  }
  render() {
    const { path, exact, componentProps } = this.props
    const { loggedIn } = this.props.auth
    const C = this.props.component

    return (
      <Route
        path={path}
        exact={exact}
        render={() =>
          loggedIn ? <C {...componentProps} /> : <Redirect to="/login" />
        }
      />
    )
  }
}

const mapStateToProps = state => ({
  auth: state.auth
})

export default withRouter(connect(mapStateToProps)(ProtectedRoute))
