import { createStore, applyMiddleware } from "redux"
import { rootReducer } from "../reducers/"
import thunk from "redux-thunk"
import { logger } from "redux-logger"
import { routerMiddleware } from "connected-react-router"
import { history } from "../history"

export const store = createStore(
  rootReducer(history),
  applyMiddleware(thunk, routerMiddleware(history), logger)
)
