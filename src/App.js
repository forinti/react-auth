import React, { Component } from "react"
import "./App.css"
import { connect } from "react-redux"
import "bootstrap/dist/css/bootstrap.css"
import { Route } from "react-router-dom"
import { AnimatedSwitch } from "react-router-transition"
import { ConnectedRouter as Router } from "connected-react-router"
import Auth from "./pages/Auth"
import { auth } from "./actions/AuthActions"
import MainPage from "./pages/Main"
import Register from "./pages/Register"
import ProtectedRoute from "./ProtectedRoute"
import "animate.css/animate.css"
import { logOut } from "./actions/AuthActions"
import { setToken } from "./actions/AuthActions"
import { history } from "./history"
import Nav from "./components/Nav"
import NotFound from "./pages/NotFound"

class NewComponent extends Component {
  constructor(props) {
    super(props)
    this.props = props
  }

  render() {
    return (
      <div>
        <div> Hi, this is another component </div>
        <div> Hi, this is another component </div>
        <div> Hi, this is another component </div>
        <div> Hi, this is another component </div>
        <div> Hi, this is another component </div>
        <div> Hi, this is another component </div>
        <div> Hi, this is another component </div>
      </div>
    )
  }
}

class App extends Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = { clicked: false }
  }

  componentDidMount() {
    if (localStorage.getItem("_token") != null) {
      this.props.setToken(localStorage.getItem("_token"))
    }
  }

  handleLogoutClick = e => {
    e.preventDefault()
    this.props.logOut()
  }

  render() {
    const { loggedIn, user } = this.props.auth
    return (
      <Router history={history}>
        <div className="App">
          <Nav loggedIn={loggedIn} logOutAction={this.props.logOut} />
          <div className="container">
            <div>
              <AnimatedSwitch
                atEnter={{ opacity: 0, offset: 2000 }}
                atLeave={{ offset: -100, opacity: 0 }}
                atActive={{ opacity: 1, offset: 0 }}
                mapStyles={styles => ({
                  transform: `translateY(${styles.offset}%)`,
                  opacity: styles.opacity
                })}
                className="switch-wrapper"
              >
                <Route path="/login" exact component={Auth} />
                <Route path="/register" exact component={Register} />
                <Route path="/new" auth={auth} component={NewComponent} />
                <ProtectedRoute
                  componentProps={{ user: user }}
                  exact={true}
                  path={"/"}
                  component={MainPage}
                />
                <Route exact component={NotFound} />
              </AnimatedSwitch>
            </div>
          </div>
        </div>
      </Router>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    router: state.router
  }
}

const mapDispatchToProps = dispatch => {
  return {
    authRequest: credentials => dispatch(auth(credentials)),
    setToken: token => dispatch(setToken(token)),
    logOut: () => dispatch(logOut())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
