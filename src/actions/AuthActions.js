import axios from "axios"
import { api } from "../api"
import { push } from "connected-react-router"
import { config, ENVIRONMENT_DUMMY } from '../environment/config';

export const AUTH = "AUTH_REQUEST"
export const AUTH_SUCCESS = "AUTH_SUCCESS"
export const AUTH_FAILURE = "AUTH_FAILURE"
export const SET_TOKEN = "SET_AUTH_TOKEN"
export const SET_USER = "SET_USER"
export const LOG_OUT = "LOG_OUT"
export const REGISTER = "REGISTER_REQUEST"
export const REGISTER_SUCCESS = "REGISTER_SUCCESS"
export const REGISTER_FAILURE = "REGISTER_FAILURE"

var Thread = {
  sleep: function(ms) {
    var start = Date.now()

    while (true) {
      var clock = Date.now() - start
      if (clock >= ms) break
    }
  }
}

export function register(credentials) {
  return dispatch => {
    dispatch({ type: REGISTER });
    if (config.environment === ENVIRONMENT_DUMMY) {
        Thread.sleep(1000)
        dispatch({
            type: REGISTER_SUCCESS,
            payload: {user: { login: credentials.login }}
        })
    } else {
        axios
            .post(api.register, credentials)
            .then(response => {
                const {user, token} = response.data
                dispatch({
                    type: REGISTER_SUCCESS,
                    payload: {user: user}
                })
                dispatch({
                    type: SET_TOKEN,
                    payload: token
                })
                localStorage.setItem("_token", token)
            })
            .catch(error => {
                dispatch({
                    type: REGISTER_FAILURE,
                    payload: error.response.data.errors
                })
            })
    }
  }
}

export function auth(credentials) {
  return dispatch => {
    dispatch({ type: AUTH });
    if (config.environment === ENVIRONMENT_DUMMY) {
        Thread.sleep(1000)
          dispatch({
              type: AUTH_SUCCESS,
              payload: {
                  user: { login: credentials.login }
              }
          })
    } else {
        axios
            .post(api.auth, credentials)
            .then(response => {
                Thread.sleep(1000)
                const {user, token} = response.data
                dispatch({
                    type: AUTH_SUCCESS,
                    payload: {
                        user: user
                    }
                })
                dispatch({type: SET_TOKEN, payload: token})
                localStorage.setItem("_token", token)
            })
            .catch(error => {
                Thread.sleep(1000)
                const errors = error.response.data.errors
                dispatch({
                    type: AUTH_FAILURE,
                    payload: errors
                })
            })
    }
  }
}

export function setToken(token) {
  return dispatch => {
    axios
      .get(api.getuserbytoken + `?token=${token}`)
      .then(response => {
        const user = response.data.user
        dispatch({ type: AUTH_SUCCESS, payload: { user: user } })
        dispatch({ type: SET_TOKEN, payload: token })
      })
      .catch(error => {
        console.log(error)
        const status = error.response.status
        if (status === 422) localStorage.removeItem("_token")
      })
  }
}

export function logOut() {
  return dispatch => {
    localStorage.removeItem("_token")
    dispatch(push("/login"))
    dispatch({ type: LOG_OUT, payload: true })
    dispatch({ type: SET_TOKEN, payload: null })
    dispatch({ type: SET_USER, payload: null })
  }
}
